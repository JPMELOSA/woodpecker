﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Loading : MonoBehaviour
{
    public string cenaACarregar;
    public float tempoFixoSeg = 5;
    public enum TipoCarreg {Carregamento, Fixo};
    public TipoCarreg tipoCarregamento;
    public Image barraDeCarregamento;
    public TextMeshProUGUI TextoProgresso;
    private int progresso = 0;
    private string textoOriginal;

    void Start()
    {
        switch (tipoCarregamento)
        {
            case TipoCarreg.Carregamento:
                {
                    StartCoroutine(CenaDeCarregamento(cenaACarregar));
                    break;
                }
            case TipoCarreg.Fixo:
                {
                    StartCoroutine(TempoFixo(cenaACarregar));
                    break;
                }
        }
        //

        if(TextoProgresso != null)
        {
            textoOriginal = TextoProgresso.text;
        }
        if(barraDeCarregamento != null)
        {
            barraDeCarregamento.type = Image.Type.Filled;
            barraDeCarregamento.fillMethod = Image.FillMethod.Horizontal;
            barraDeCarregamento.fillOrigin = (int)Image.OriginHorizontal.Left;
        }
    }

    IEnumerator CenaDeCarregamento(string cena)
    {
        AsyncOperation carregamento = SceneManager.LoadSceneAsync(cena);
        while (!carregamento.isDone)
        {
            progresso = (int) (carregamento.progress * 100.0f);
            yield return null;
        }
    }

    IEnumerator TempoFixo(string cena)
    {
        yield return new WaitForSeconds(tempoFixoSeg);
        SceneManager.LoadScene(cena);
    }

    // Update is called once per frame
    void Update()
    {
        switch (tipoCarregamento)
        {
            case TipoCarreg.Carregamento:
                {
                    StartCoroutine(CenaDeCarregamento(cenaACarregar));
                    break;
                }
            case TipoCarreg.Fixo:
                {
                    progresso = (int) (Mathf.Clamp((Time.time / tempoFixoSeg), 0.0f, 1.0f) * 100.0f);
                    break;
                }
        }
        //

        if (TextoProgresso != null)
        {
            TextoProgresso.text = textoOriginal + " " + progresso + " %";
        }
        if (barraDeCarregamento != null)
        {
            barraDeCarregamento.fillAmount = (progresso / 100.0f);
        }
    }
}
