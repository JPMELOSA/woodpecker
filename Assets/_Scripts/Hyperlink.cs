using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class Hyperlink : MonoBehaviour
{
	[Header("URL")]
	[SerializeField] private string url;

	public void OpenUrl()
	{
		Application.OpenURL(url);
	}    
}

/*
String url = "https://api.whatsapp.com/send?phone="+ number +"&text=" + message;
//Number variable needs to include the country code. If you want to send a message to Guatemala
// and the mobile number is: 12345678, your final string will be:
// https://api.whatsapp.com/send?phone=50212345678&text=hello
"https://api.whatsapp.com/send?phone=+5535984178494&text=Woody Woodypecker"

*/