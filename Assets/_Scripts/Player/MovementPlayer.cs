﻿using System.Collections;
using UnityEngine;

public class MovementPlayer : MonoBehaviour
{
	[Header("Sprites Player")]
	[SerializeField] private GameObject Idle;

	[SerializeField] public GameObject Walk;

	[SerializeField] private GameObject jumpUp;

	[SerializeField] private GameObject fall;
	
	[SerializeField] private GameObject Dead;
	
	[Header("Vars")]
	Rigidbody2D rigid;

	[Range(100, 900)]
	[SerializeField] private float speed = 550f;

	[Range(1000, 3000)]
	[SerializeField] private float jumpForce = 2000f;

	public float movement;

	public bool jumping = false;
	
	public bool jump;

	[Header("Trails")]
	[SerializeField] private GameObject prefabTrail;

	[SerializeField] private GameObject leftJumpPrefab;

	[SerializeField] private GameObject rightJumpPrefab;

	[SerializeField] private float metersFloor = 0;

	[Range(1, 10)]
	[SerializeField] private float cycleValue = 3f;
	
	[Range(-5, 5)]
	[SerializeField] private float distanceFromTheTrailX = 0.8f;
	
	[Range(-5, 5)]
	[SerializeField] private float distanceFromTheTrailY = -0.1f;
	
	[Range(-5, 5)]
	[SerializeField] private float distanceFromTheDustY = -0.18f;

	private float lastValueX;
	
	private bool canInstantiateTrace;

	private float previousPosition;

	private bool walkingTrail;

	public bool isMobile;

	void Start()
	{
		SufferedGivingRecent();
		if(GameMode.GM.gameMode == GameMode.MODE.Mobile)
		{
			isMobile = true;
		}
		else
		{
			isMobile = false;
		}
		rigid = GetComponent<Rigidbody2D>();
		jump = false;
		walkingTrail = true;
	}

	void Update()
	{
		checkPlayerSide();
		SufferedGivingRecent();
		if (!Stats.stats.gameOver)
		{
			if (isMobile)
			{
				MobileMovement();
			}
			else
			{
				Movement();
				ChangeSprite();
				Jump();
			}
			InstantiateTrace();
		}
		CheckGameOver();
	}

	#region Movement
	private void MobileMovement()
	{
		if (movement != 0 && jumping == false)
		{
			Walking();
			walkingTrail = true;
		}
		if (movement == 0 && jumping == false)
		{
			Stopped();
			walkingTrail = false;
		}
		if (jumping == true)
		{
			CheckJump(transform.position.y);
			walkingTrail = false;
		}
		speedForce();
		InvertImage();
	}
 
	public void Jump()
	{
		if(jump)
		{
			if (!jumping)
			{
				rigid.AddForce(Vector2.up * jumpForce);
				jumping = true;
				JumpUp();
			}
		}
	}

	private void Movement()
	{
		movement = Input.GetAxisRaw("Horizontal");
		speedForce();
		InvertImage();
		if (Input.GetKeyDown(KeyCode.Space))
		{
			if (jumping == false)
			{
				jumpForceFunc();
			}
		}
	}
	
	private void jumpForceFunc()
	{
		AudioManager.Audio.PlayEffects("JUMP");
		rigid.AddForce(Vector2.up * jumpForce * Time.fixedDeltaTime, ForceMode2D.Impulse);
	}

	private void speedForce()
	{
		rigid.velocity = new Vector2(movement * speed * Time.fixedDeltaTime, rigid.velocity.y);
	}
	
	private void checkPlayerSide()
	{
		if(movement == -1)
		{
			Stats.stats.isRight = false;
		}
		if(movement == 1)
		{
			Stats.stats.isRight = true;
		}
	}
	
	#endregion Movement

	
	#region Traces
	
	private void InstantiateTraces()
	{
		jumping = false;
		GameObject leftJump  = Instantiate(leftJumpPrefab, new Vector3(transform.position.x - distanceFromTheTrailX, transform.position.y - distanceFromTheTrailY, transform.position.z), Quaternion.Euler(0, 0, 35f));
		GameObject rightJump = Instantiate(rightJumpPrefab, new Vector3(transform.position.x + distanceFromTheTrailX, transform.position.y - distanceFromTheTrailY, transform.position.z), Quaternion.Euler(0, 0, -35f));

		Destroy(leftJump, 0.45f);
		Destroy(rightJump, 0.45f);
		canInstantiateTrace = true;
	}

	 private void InstantiateTrace()
	{
		if(canInstantiateTrace == true)
		{
			if (Idle.activeSelf == false)
			{
				float valueX = Mathf.Abs(transform.position.x);

				valueX = valueX - lastValueX;

				if (valueX < 0)
				{
					valueX = Mathf.Abs(valueX);
				}

				metersFloor = metersFloor + valueX;

				if (metersFloor > cycleValue)
				{
					if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A) || walkingTrail)
					{
						if (!jumping)
						{
							GameObject dust = Instantiate(prefabTrail, new Vector3(transform.position.x, transform.position.y - distanceFromTheDustY, transform.position.z), Quaternion.identity);
							Destroy(dust, 1f);
							metersFloor = 0;
						}
					}
				}
				lastValueX = Mathf.Abs(transform.position.x);
			}
		}
	}
	#endregion Traces
	

	#region OnTrigger
	
	private void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag == "Piso")
		{
			jumping = true;
		}
		if (other.tag == "Barco")
		{
			jumping = true;
		}
		if(other.tag == "Inimigo")
		{
			jumping = true;
		}
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Piso")
		{
			InstantiateTraces();
		}
		if(other.tag == "Barco")
		{
			jumping = false;
			Stopped();
			canInstantiateTrace = false;
		}
		if(other.tag == "Inimigo")
		{
			jumping = false;
			canInstantiateTrace = false;
		}

		if(other.tag == "Explosao")
		{
			Debug.Log("Buum");
		}
	}
	
	private void OnTriggerStay2D(Collider2D other)
	{
		if (other.tag == "Piso")
		{
			jumping = false;
		}
		if (other.tag == "Barco")
		{
			jumping = false;
			Stopped();
		}
		if (other.tag == "Inimigo")
		{
			jumping = false;
		}
		if (other.tag == "Cactus")
		{
			jumping = false;
		}
	}
	
	#endregion OnTrigger
	
	
	#region Sprites
	
	private void ChangeSprite()
	{
		if (jumping)
		{
			CheckJump(transform.position.y);
		}
		else
		{
			if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A))
			{
				Walking();
			}
			else
			{
				Stopped();
			}
		}
	}
	
	private void CheckJump (float currentPosition)
	{
		if(previousPosition > currentPosition)
		{
			ToFall();
		}
		if(previousPosition < currentPosition)
		{
			JumpUp();
		}
		previousPosition = currentPosition;
	}
	
	private void InvertImage()
	{
		if (movement < 0)
		{
			//vira para a esquerda
			Walk.transform.eulerAngles = new Vector2(0, 180);
			jumpUp.transform.eulerAngles = new Vector2(0, 180);
			fall.transform.eulerAngles = new Vector2(0, 180);
			Idle.transform.eulerAngles = new Vector2(0, 180);
		}
		if (movement > 0)
		{
			//vira para a direita
			Walk.transform.eulerAngles = new Vector2(0, 0);
			jumpUp.transform.eulerAngles = new Vector2(0, 0);
			fall.transform.eulerAngles = new Vector2(0, 0);
			Idle.transform.eulerAngles = new Vector2(0, 0);
		}
	}
	
	private void Stopped()
	{
		Idle.SetActive(true);
		//...
		Walk.SetActive(false);
		jumpUp.SetActive(false);
		fall.SetActive(false);
	}

	private void Walking()
	{
		Walk.SetActive(true);
		//...
		Idle.SetActive(false);
		jumpUp.SetActive(false);
		fall.SetActive(false);
	}

	private void JumpUp()
	{
		jumpUp.SetActive(true);
		//...
		Walk.SetActive(false);
		Idle.SetActive(false);
		fall.SetActive(false);
	}

	private void ToFall()
	{
		fall.SetActive(true);
		//...
		Walk.SetActive(false);
		Idle.SetActive(false);
		jumpUp.SetActive(false);
	}
	
	private void Died()
	{
		Dead.SetActive(true);
		// ...
		fall.SetActive(false);
		Walk.SetActive(false);
		Idle.SetActive(false);
		jumpUp.SetActive(false);
	}
	
	#endregion Sprites
	
	#region Dano
		private void SufferedGivingRecent()
		{
			if(Stats.stats.sufferedGivingRecent)
			{
				if(Idle.activeSelf)
				{
					Idle.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 70);
					Idle.GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 70);
				}
				if(Walk.activeSelf)
				{
					Walk.GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 70);
				}
				if(jumpUp.activeSelf)
				{
					jumpUp.GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 70);
				}
				if(fall.activeSelf)
				{
					fall.GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 70);
				}
				StartCoroutine(RemoveTransparency());
			}
			else
			{
				Debug.Log("Error");
			}
		}
		
		IEnumerator RemoveTransparency()
		{
			yield return new WaitForSeconds(2);
			Idle.GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 255);
			Walk.GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 255);
			jumpUp.GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 255);
			fall.GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 255);
		} 
		
		
		private void CheckGameOver()
		{
			if(Stats.stats.gameOver)
			{
				Died();
			}
		}
		
	#endregion Dano
}