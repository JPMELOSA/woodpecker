﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerAtaque : MonoBehaviour
{
	[Header("Guns")]
	[SerializeField] private GameObject bomb;

	[SerializeField] private GameObject cake;
	
	[SerializeField] private GameObject player;

	public bool isMobile;

	void Update()
	{
		ModoDeAtaque();
	}

	private void ataque()
	{
		if (!Stats.stats.gameOver)
		{
			if (Input.GetMouseButtonDown(0))
			{
				if (Stats.stats.GetBombs() > 0)
				{
					GameObject bombs = Instantiate(bomb, transform.position, transform.rotation * Quaternion.Euler(0f, player.transform.eulerAngles.y, 0f));
					Stats.stats.SetBombs(-1);
				}
				else
				{
					AudioManager.Audio.PlayVoices("VAZIO");
				}
			}

			if (Input.GetMouseButtonDown(1))
			{
				if (Stats.stats.GetCakes() > 0)
				{
					GameObject cakes = Instantiate(cake, transform.position, transform.rotation * Quaternion.Euler(0f, player.transform.eulerAngles.y, 0f));
					Stats.stats.SetCakes(-1);
				}
				else
				{
					AudioManager.Audio.PlayVoices("VAZIO");
				}
			}
		}
	}

	public void MobileBombsAttack()
	{
		if (!Stats.stats.gameOver)
		{
			if (Stats.stats.GetBombs() > 0)
			{
				GameObject bombs = Instantiate(bomb, transform.position, transform.rotation * Quaternion.Euler(0f, player.transform.eulerAngles.y, 0f));
				Stats.stats.SetBombs(-1);
			}
			else
			{
				AudioManager.Audio.PlayVoices("VAZIO");
			}
		}
	}
	
	public void MobileCakesAttack()
	{
		if (!Stats.stats.gameOver)
		{
			if (Stats.stats.GetCakes() > 0)
			{
				GameObject cakes = Instantiate(cake, transform.position, transform.rotation * Quaternion.Euler(0f, player.transform.eulerAngles.y, 0f));
				Stats.stats.SetCakes(-1);
			}
			else
			{
				AudioManager.Audio.PlayVoices("VAZIO");
			}
		}
	}

	// --------------------------------------------------
	public void modo(bool isMobileMode)
	{
		if (isMobileMode)
		{
			isMobile = true;
		}
		else
		{
			isMobile = false;
		}
	}

	private void ModoDeAtaque()
	{
		if (isMobile)
		{
			//ataqueMobileBombas();
			//ataqueMobileTortas();
		}
		else
		{
			ataque();
		}
	}
}
