﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonManager : MonoBehaviour
{
	#region STATIC BUTTON MANAGER
	public static ButtonManager BTN;

	private void OnEnable()
	{
		if (ButtonManager.BTN == null)
		{
			ButtonManager.BTN = this;
		}
		else
		{
			if (ButtonManager.BTN != this)
			{
				Destroy(this.gameObject);
			}
		}
		//DontDestroyOnLoad(this.gameObject);
	}
	#endregion STATIC BUTTON MANAGER

	[SerializeField] private MovementPlayer _movement;

	[SerializeField] private PlayerAtaque _attack;
	
	void Start()
    {
        StartGameMode();
    }
	
	#region Initialize
	
	 public void StartGameMode()
	{
		switch (GameMode.GM.gameMode) 
		{
			case GameMode.MODE.Desktop:
				{
					this.gameObject.SetActive(false);
					break;
				}
			case GameMode.MODE.Mobile:
				{
					this.gameObject.SetActive(true);
					break;
				}
		}
	}
	
	#endregion Initialize

	#region Movement
	
	public void _BtnMoveRight(bool opc)
	{
		if (opc)
		{
			_movement.movement = 1;
		}
		else
		{
			_movement.movement = 0;
		}
	}

	public void _BtnMoveLeft(bool opc)
	{
		if (opc)
		{
			_movement.movement = -1;
		}
		else
		{
			_movement.movement = 0;
		}
	}

	public void _BtnJump(bool opc)
	{
		if (opc)
		{
			_movement.jump = true;
			_movement.Jump();
		}
		else
		{
			_movement.jump = false;
		}
	}
	
	#endregion Movement
	
	#region Attack
	
	public void _BtnAttackBombs()
	{
		if(_attack != null)
		{
			_attack.MobileBombsAttack();
		}
	}

	public void _BtnAttackCakes()
	{
		if (_attack != null)
		{
			_attack.MobileCakesAttack();
		}
	}
	
	
	#endregion Attack
}
