﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMode : MonoBehaviour
{
	#region STATIC GAME MODE
	public static GameMode GM;

	private void OnEnable()
	{
		if (GameMode.GM == null)
		{
			GameMode.GM = this;
		}
		else
		{
			if (GameMode.GM != this)
			{
				Destroy(this.gameObject);
			}
		}
		DontDestroyOnLoad(this.gameObject);
	}
	#endregion STATIC GAME MODE
	
	public enum MODE { Desktop, Mobile }
	
	public enum Language { Portuguese, English }

	public enum Difficulty { easy, moderate, hard }


	[Header("GAME MODE")]
	public MODE gameMode;
	
	[Header("LANGUAGE")]
	public Language _language = Language.English;
	
	[Header("DIFFICULTY")]
	public Difficulty _difficulty = Difficulty.moderate;
}