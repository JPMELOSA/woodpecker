using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
	#region STATIC STATS
	public static UIManager UIC;

	private void OnEnable()
	{
		if (UIManager.UIC == null)
		{
			UIManager.UIC = this;
		}
		else
		{
			if (UIManager.UIC != this)
			{
				Destroy(this.gameObject);
			}
		}
		//DontDestroyOnLoad(this.gameObject);
	}
	#endregion STATIC STATS


	[Header("SCORE")]
	[SerializeField] private Text score;

	[Header("GUNS")]
	[SerializeField] private Text cakes;

	[SerializeField] private Text bomb;

	[Header("LIFE")]

	[SerializeField] private GameObject[] life;

	[SerializeField] private GameObject[] noLife;

	[Header("BAG")]

	[SerializeField] private GameObject[] bag;

	[SerializeField] private GameObject[] noBag;

	[Header("Menus")]

	[SerializeField] private GameObject restartMenu;

	[SerializeField] private GameObject initialMenu;

	[SerializeField] private GameObject pauseMenu;

	#region UI

	public void SendScoreToUi(int scoreValue)
	{
		score.text = scoreValue.ToString();
	}

	public void SendCakesToUi(int cakesValue)
	{
		cakes.text = cakesValue.ToString();
	}

	public void SendBombsToUi(int bombsValue)
	{
		bomb.text = bombsValue.ToString();
	}

	public void SendLifesToUI(int lifesValue)
	{
		for (int i = 0; i < life.Length; i++)
		{
			if(i < lifesValue)
			{
				life[i].SetActive(true);
				noLife[i].SetActive(false);
			}
			else
			{
				life[i].SetActive(false);
				noLife[i].SetActive(true);
			}
		}
	}

	public void SendBagsToUI(int bagNumber)
	{
		switch (bagNumber)
		{
			case 1:
				{
					noBag[0].SetActive(false);
					bag[0].SetActive(true);
					break;
				}
			case 2:
				{
					noBag[1].SetActive(false);
					bag[1].SetActive(true);
					break;
				}
			case 3:
				{
					noBag[2].SetActive(false);
					bag[2].SetActive(true);
					break;
				}
			case 4:
				{
					noBag[3].SetActive(false);
					bag[3].SetActive(true);
					break;
				}
			case 5:
				{
					noBag[4].SetActive(false);
					bag[4].SetActive(true);
					break;
				}
			default:
				{
					Debug.LogError("Numero da Bag errado!");
					break;
				}
		}
	}

	#endregion UI

	#region MENU

	public void RestartMenu(bool activateMenu, float timeoutToCallTheRestartMenu = 0)
	{
		StartCoroutine(timeoutRestartMenu(activateMenu, timeoutToCallTheRestartMenu));
	}
	
	IEnumerator timeoutRestartMenu(bool activateMenu, float timeoutToCallTheRestartMenu)
	{
		yield return new WaitForSeconds(timeoutToCallTheRestartMenu);
		restartMenu.SetActive(activateMenu);
	}

	public void InitialMenu(bool activateMenu)
	{
		initialMenu.SetActive(activateMenu);
	}

	public void PauseMenu(bool activateMenu)
	{
		pauseMenu.SetActive(activateMenu);
		// Mudar o Tempo AQUI
	}

	#endregion MENU
}
