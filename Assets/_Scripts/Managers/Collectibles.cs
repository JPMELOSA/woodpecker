using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectibles : MonoBehaviour
{
	[Header("Collectibles")]

	[SerializeField] private CollectiblesType collectibles;

	[Range(1, 10)]
	[SerializeField] private int amount = 1;

	[Range(1, 10000)]
	[SerializeField] private int valueOfThisItem = 100;

	[Header("Movement")]

	[Range(0, 5)]
	[SerializeField] private float movementSpeed = 0;

	[SerializeField] private float movement = 0;

	[SerializeField] private float max = 0.4f;

	[SerializeField] private float min =  0.7f;

	float startingPosition = 0;

	private Rigidbody2D rigid;

	void Start()
	{
		rigid = GetComponent<Rigidbody2D>();
		movementSpeed = 1.1f;
		movement = 1;
		startingPosition = transform.position.y;
	}

	void Update()
	{
		movimentacao();
	}

	private void movimentacao()
	{
		LimiteMovimentacao();
		rigid.velocity = new Vector2(rigid.velocity.x, movement * movementSpeed);
	}

	private void LimiteMovimentacao()
	{
		if (transform.position.y <= startingPosition - min)
		{
			movement = 1;
		}
		if (transform.position.y >= startingPosition + max)
		{
			movement = -1;
		}
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.tag == "Player")
		{
			switch (collectibles)
			{
				case CollectiblesType.Bomb:
					{
						Stats.stats.SetBombs(amount);
						AudioManager.Audio.PlayEffects("LIFE");
						break;
					}
				case CollectiblesType.Cake:
					{
						Stats.stats.SetCakes(amount);
						AudioManager.Audio.PlayEffects("LIFE");
						break;
					}
				case CollectiblesType.Life:
					{
						Stats.stats.SetLife(amount);
						AudioManager.Audio.PlayEffects("LIFE");
						break;
					}
				case CollectiblesType.Bag1:
					{
						Stats.stats.SetBags(amount, 1);
						AudioManager.Audio.PlayVoices("LAUGH");
						break;
					}
				case CollectiblesType.Bag2:
					{
						Stats.stats.SetBags(amount, 2);
						AudioManager.Audio.PlayVoices("LAUGH");
						break;
					}
				case CollectiblesType.Bag3:
					{
						Stats.stats.SetBags(amount, 3);
						AudioManager.Audio.PlayVoices("LAUGH");
						break;
					}
				case CollectiblesType.Bag4:
					{
						Stats.stats.SetBags(amount, 4);
						AudioManager.Audio.PlayVoices("LAUGH");
						break;
					}
				case CollectiblesType.Bag5:
					{
						Stats.stats.SetBags(amount, 5);
						AudioManager.Audio.PlayVoices("LAUGH");
						break;
					}
				case CollectiblesType.StrawberryCake:
					{
						Stats.stats.SetLife();
						AudioManager.Audio.PlayVoices("STRAWBERRYCAKE");
						break;
					}
				case CollectiblesType.PinkEyeWhiskey:
					{
						AudioManager.Audio.PlayVoices("DRINKING");
						break;
					}
				case CollectiblesType.BagOfGold:
					{
						AudioManager.Audio.PlayEffects("COIN");
						break;
					}
				case CollectiblesType.Popcorn:
					{
						AudioManager.Audio.PlayVoices("EICARAVCGOSTADEPIPOCA"); 
						break;
					}
				default:
					{
						Debug.LogError("Opção Inválida");
						break;
					}
			}
			Stats.stats.SetScore(valueOfThisItem);
			Destroy(this.gameObject);
		}
	}
}


enum CollectiblesType
{
	Bomb,
	Cake,
	Life,
	Bag1,
	Bag2,
	Bag3,
	Bag4,
	Bag5,
	StrawberryCake,
	PinkEyeWhiskey,
	BagOfGold,
	Popcorn
}
