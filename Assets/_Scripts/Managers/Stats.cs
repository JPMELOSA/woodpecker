using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{
	#region STATIC STATS
	public static Stats stats;

	private void OnEnable()
	{
		if (Stats.stats == null)
		{
			Stats.stats = this;
		}
		else
		{
			if (Stats.stats != this)
			{
				Destroy(this.gameObject);
			}
		}
		//DontDestroyOnLoad(this.gameObject);
	}
	#endregion STATIC STATS

	[Header("Score")]
	[SerializeField] private int score;

	[Header("Guns")]
	[SerializeField] private int bombs;

	[SerializeField] private int cakes;

	[Header("Life")]
	[SerializeField] private int lifes;
	
	public bool sufferedGivingRecent = false;

	[Header("Bags")]
	[SerializeField] private int bags;

	[Header("Game Over")] // Mudar para outro Manager
	public bool gameOver;

	[Header("Pause")] // Mudar para outro Manager
	public bool pause;
	
	[Header("Player Side")]
	public bool isRight;

	void Start()
	{
		SetLife(3);
		SetScore(0);
		SetCakes(0);
		SetBombs(0);
	}

	#region SCORE

	public void SetScore(int newScore)
	{
		score += newScore;
		UIManager.UIC.SendScoreToUi(score);
	}

	public int GetScore()
	{
		return score;
	}
	#endregion SCORE

	#region GUNS

	public void SetBombs(int newBombs = 1)
	{
		bombs += newBombs;
		UIManager.UIC.SendBombsToUi(bombs);
	}

	public int GetBombs()
	{
		return bombs;
	}

	//

	public void SetCakes(int newCakes = 1)
	{
		cakes += newCakes;
		UIManager.UIC.SendCakesToUi(cakes);
	}

	public int GetCakes()
	{
		return cakes;
	}

	#endregion GUNS

	#region LIFE

	public void SetLife(int newLife = 1)
	{
		if(lifes >= 3)
		{
			score += 1000;
		}
		else
		{	
			lifes += newLife;
			UIManager.UIC.SendLifesToUI(lifes);
		}
	}

	public void RemoveLife(int newLife = 1)
	{
		if(!sufferedGivingRecent)
		{	
			AudioManager.Audio.PlayVoices("AAAI");
			lifes -= newLife;
			StartCoroutine(SufferedGivingRecent());
			UIManager.UIC.SendLifesToUI(lifes);
			if(lifes <= 0)
			{
				gameOver = true;
				AudioManager.Audio.PlayEffects("WOODYDDEAD");
				UIManager.UIC.RestartMenu(true, 2f);
			}
		}
	}
	
	IEnumerator SufferedGivingRecent()
	{
		Debug.Log("Sofreu Dano");
		sufferedGivingRecent = true;
		yield return new WaitForSeconds(2);
		sufferedGivingRecent = false;
	}

	public int GetLife()
	{
		return lifes;
	}

	#endregion LIFE

	#region BAGS

	public void SetBags(int newBags = 1, int bagNumber = 0)
	{
		bags += newBags;
		UIManager.UIC.SendBagsToUI(bagNumber);
	}

	public int GetBags()
	{
		return bags;
	}
	#endregion BAGS
}
