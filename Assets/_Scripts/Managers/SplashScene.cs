using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashScene : MonoBehaviour
{
    [Range(1, 5)]
    [SerializeField] private float timeToStayOnThePage;
    void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        StartCoroutine(ChangeScene());
    }

    IEnumerator ChangeScene()
    {
        yield return new WaitForSeconds(timeToStayOnThePage);
        TransitionSceneManager.TSC.TransitionScene("02-MainMenu", 2);
    }
}
