using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
	#region STATIC AUDIO MANAGER
	public static AudioManager Audio;

	private void OnEnable()
	{
		if (AudioManager.Audio == null)
		{
			AudioManager.Audio = this;
		}
		else
		{
			if (AudioManager.Audio != this)
			{
				Destroy(this.gameObject);
			}
		}
		DontDestroyOnLoad(this.gameObject);
	}
	#endregion STATIC AUDIO MANAGER

	[Header("Audios Sources")]
	[SerializeField] public AudioSource music; // Musica
	[SerializeField] private AudioSource effects; // som artificiais
	[SerializeField] private AudioSource voice; // Narra��o e voz do pica pau e etc
	[SerializeField] private AudioSource Ambiences; // Paisagem sonora, todo som que acontece ao redor da cena, som de vizinho, pads, buzinha ao longe, tudo que est� em volta da cena, ar condicionado
	[SerializeField] private AudioSource Foley; // Exlplos�es e etc

	[Header("MUSICS")]
	[SerializeField] private AudioClip musicMainMenu;
	[SerializeField] private AudioClip musicEvergladeRaid;
	[SerializeField] private AudioClip endSoundTrack;

	[Header("EFFECTS")]
	[SerializeField] private AudioClip mouseClick;
	[SerializeField] private AudioClip tookAnItem;
	[SerializeField] private AudioClip life;
	[SerializeField] private AudioClip jump;
	[SerializeField] private AudioClip Shot; 
	[SerializeField] private AudioClip coin;
	[SerializeField] private AudioClip woodyDead; 
	[SerializeField] private AudioClip BatidaForte; 
	
	[Header("VOICES - Woodypecker")]
	[SerializeField] private AudioClip StrawberryCake;
	[SerializeField] private AudioClip Drinking;
	[SerializeField] private AudioClip Aaaai;
	[SerializeField] private AudioClip laugh;
	[SerializeField] private AudioClip JobForTourists;
	[SerializeField] private AudioClip PopCornWoody; 
	// Inimigos
	
	[Header("WildBillHiccup")]
	[SerializeField] private AudioClip WildBillHiccupGroan;
	[SerializeField] private AudioClip WildBillHiccupDead;
	
	[Header("Meany")]
	[SerializeField] private AudioClip MeanyGroan;
	[SerializeField] private AudioClip MeanyDead;
	
	[Header("WallyWalrus")]
	[SerializeField] private AudioClip WallyWalrusSpat;
	[SerializeField] private AudioClip WallyWalrusDead;
	[SerializeField] private AudioClip WallyWalrusCoughing;
	
	[Header("Frank")]
	[SerializeField] private AudioClip FrankGroan;
	[SerializeField] private AudioClip FrankDead;
	[SerializeField] private AudioClip FrankSound;
	
	[Header("BuzzBuzzard")]
	[SerializeField] private AudioClip BuzzBuzzardGroan;
	[SerializeField] private AudioClip BuzzBuzzardDead;
	[SerializeField] private AudioClip BuzzBuzzardGood;
	
	[Header("Jubileu")]
	[SerializeField] private AudioClip jubileuDead;
	
	[Header("GabbyGator")]
	[SerializeField] private AudioClip GabbyGatorTalking;
	[SerializeField] private AudioClip GabbyGatorHey;
	[SerializeField] private AudioClip GabbyGatorDead;
	
	[Header("Doley")]
	[SerializeField] private AudioClip AngryDoley;
	
	[Header("Duck")]
	[SerializeField] private AudioClip DuckGroan;
	
	[Header("Narration")]
	// Level 1
	[SerializeField] private AudioClip FreeBoat;
	[SerializeField] private AudioClip Empty;
	[SerializeField] private AudioClip TouristInformation;

	[Header("FOLEY")]
	[SerializeField] private AudioClip Explosion;
	[SerializeField] private AudioClip shot;
	[SerializeField] private AudioClip Cake;
	[SerializeField] private AudioClip onGround;

	// Volumes
	[Header("VALUES")]
	
	[Range(0, 1)]
	public float musicVolume;
	
	[Range(0, 1)]
	public float effectsVolume;
	
	[Range(0, 1)]
	public float voiceVolume;
	
	[Range(0, 1)]
	public float foleyVolume;
	
	public void PlayMusic(string musicName)
	{
		music.volume = musicVolume;
		switch (musicName)
		{
			case "MAINMENU":
				{
					music.clip = musicMainMenu;
					music.Play();
					break;
				}
			case "EVERGLADES":
				{
					music.clip = musicEvergladeRaid;
					music.Play();
					break;
				}
			case "THEEND":
				{
					music.clip = endSoundTrack;
					music.Play();
					break;
				}
			case "STOP":
				{
					music.Stop();
					break;
				}
		}
	}


	public void PlayEffects(string effectsName)
	{
			switch (effectsName)
			{
				case "MOUSECLICK":
					{
						effects.PlayOneShot(mouseClick, effectsVolume);
						break;
					}
				case "TOOKITEM":
					{
						effects.PlayOneShot(tookAnItem, effectsVolume);
						break;
					}
				case "LIFE":
					{
						effects.PlayOneShot(life, effectsVolume);
						break;
					}
				case "JUMP":
					{
						effects.PlayOneShot(jump, effectsVolume);
						break;
					}
				case "SHOT":
					{
						effects.PlayOneShot(Shot, effectsVolume);
						break;
					}
				case "COIN":
					{
						effects.PlayOneShot(coin, effectsVolume);
						break;
					}
				case "WOODYDDEAD":
					{
						effects.PlayOneShot(woodyDead, effectsVolume);
						break;
					}
				case "StrongBeat":
				
					{
						effects.PlayOneShot(BatidaForte, effectsVolume);
						break;
					}
			}
	}

	public void PlayVoices(string VoiceName)
	{
		switch (VoiceName)
		{
			case "STRAWBERRYCAKE": // Woodypecker
				{
					voice.PlayOneShot(StrawberryCake, voiceVolume);
					break;
				}
			case "DRINKING":
				{
					voice.PlayOneShot(Drinking, voiceVolume);
					break;
				}
			case "AAAI":
				{
					voice.PlayOneShot(Aaaai, voiceVolume);
					break;
				}
			case "LAUGH":
				{
					voice.PlayOneShot(laugh, voiceVolume);
					break;
				}
			case "BICOPARATURISTAS":
				{
					voice.PlayOneShot(JobForTourists, voiceVolume);
					break;
				}
			case "EICARAVCGOSTADEPIPOCA":
				{
					voice.PlayOneShot(PopCornWoody, voiceVolume);
					break;
				}
			case "WILDDEAD": // Wild
				{
					voice.PlayOneShot(WildBillHiccupDead, voiceVolume);  
					break;
				}
			case "WILDGROAN":
				{
					voice.PlayOneShot(WildBillHiccupGroan, voiceVolume);
					break;
				}
			case "MEANYGROAN": // Meany
				{
					voice.PlayOneShot(MeanyGroan, voiceVolume);
					break;
				}
			case "MEANYDEAD": 
				{
					voice.PlayOneShot(MeanyDead, voiceVolume);
					break;
				}
			case "WALLYSPAT": // Wally
				{
					voice.PlayOneShot(WallyWalrusSpat, voiceVolume);
					break;
				}
			case "WALLYDEAD":
				{
					voice.PlayOneShot(WallyWalrusDead, voiceVolume);
					break;
				}
			case "WALLYCOUGHING":
				{
					voice.PlayOneShot(WallyWalrusCoughing, voiceVolume);
					break;
				}
			case "FrankGroan": // Frank
				{
					voice.PlayOneShot(FrankGroan, voiceVolume);
					break;
				}
			case "FrankDead":
				{
					voice.PlayOneShot(FrankDead, voiceVolume);
					break;
				}
			case "FrankSound":
				{
					voice.PlayOneShot(FrankSound, voiceVolume);
					break;
				}
			case "BuzzBuzzardGroan": // Buzz
				{
					voice.PlayOneShot(BuzzBuzzardGroan, voiceVolume);
					break;
				}
			case "BuzzBuzzardDead":
				{
					voice.PlayOneShot(BuzzBuzzardDead, voiceVolume);
					break;
				}
			case "BuzzBuzzardGood":
				{
					voice.PlayOneShot(BuzzBuzzardGood, voiceVolume);
					break;
				}
			case "JUBILEUDEAD": // Jubileu
				{
					voice.PlayOneShot(jubileuDead, voiceVolume); 
					break;
				}
			case "GABBYGATORTALKING": // Gabby
				{
					voice.PlayOneShot(GabbyGatorTalking, voiceVolume);
					break;
				}
			case "GABBYGATORHEY":
				{
					voice.PlayOneShot(GabbyGatorHey, voiceVolume);
					break;
				}
			case "DOLEY1": // DOLEY
				{
					voice.PlayOneShot(AngryDoley, voiceVolume);
					break;
				}
			case "GABBYGATORDEAD":
				{
					voice.PlayOneShot(GabbyGatorDead, voiceVolume);
					break;
				}
			case "DUCKGROAN": // Duck
				{
					voice.PlayOneShot(DuckGroan, voiceVolume);
					break;
				}
			case "BARCOGRATIS": // Narration
				{
					voice.PlayOneShot(FreeBoat, voiceVolume);
					break;
				}
			case "VAZIO":
				{
					voice.PlayOneShot(Empty, voiceVolume);
					break;
				}
			case "INFORMATION":
				{
					voice.PlayOneShot(TouristInformation, voiceVolume);
					break;
				}
			default:
				{
					Debug.LogError("Audio Voice Inválido");
					break;
				}
		}
	}

	public void PlayFoley(string FoleyName)
	{
		switch (FoleyName)
		{
			case "EXPLOSION":
				{
					Foley.PlayOneShot(Explosion, foleyVolume);
					break;
				}
			case "SHOT":
				{
					Foley.PlayOneShot(shot, foleyVolume);
					break;
				}
			case "TORTA":
				{
					Foley.PlayOneShot(Cake, foleyVolume);
					break;
				}
			case "ONGROUND":
				{
					Foley.PlayOneShot(onGround, foleyVolume);
					break;
				}
			default:
				{
					Debug.LogError("AUDIO FOLEY INVÁLIDO");
					break;
				}
		}
	}
}
