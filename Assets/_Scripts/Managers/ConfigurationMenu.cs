using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfigurationMenu : MonoBehaviour
{
	[Header("SLIDERS")]
	
	[SerializeField] private Slider MusicSlider;
	
	[SerializeField] private Slider VoiceSlider;
	
	[SerializeField] private Slider EffectsSlider;
	
	[SerializeField] private GameObject configurationMenu;
	
	[Header("TRACES")]
	
	[SerializeField] private GameObject easyTrace;
	[SerializeField] private GameObject moderateTrace;
	[SerializeField] private GameObject hardTrace;
	void Start()
	{
		LoadVolumes();
		LoadDifficulty();
	}
	
	public void _OpenConfigurationMenu()
	{
		configurationMenu.SetActive(true);
		LoadDifficulty(true);
		LoadVolumes(true);
	}
	
	public void _CloseConfigurationMenu()
	{
		configurationMenu.SetActive(false);
		SaveVolumes();
		SaveDifficulty(); 
	}
	
	#region AUDIO
	
	public void MusicVolume(float _MusicVolume)
	{
		AudioManager.Audio.musicVolume = _MusicVolume;
		AudioManager.Audio.music.volume = AudioManager.Audio.musicVolume;
	}
	
	public void VoiceVolume(float _VoiceVolume)
	{
		AudioManager.Audio.voiceVolume = _VoiceVolume;
	}
	
	public void EffectsVolume(float _EffectsVolume)
	{
		AudioManager.Audio.effectsVolume = _EffectsVolume;
		AudioManager.Audio.foleyVolume = _EffectsVolume;
	}
	
	private void SaveVolumes()
	{
		PlayerPrefs.SetFloat("MUSICVOLUME", AudioManager.Audio.music.volume);
		PlayerPrefs.SetFloat("VOICEVOLUME", AudioManager.Audio.voiceVolume);
		PlayerPrefs.SetFloat("EFFECTSVOLUME", AudioManager.Audio.effectsVolume);
	}
	
	public void LoadVolumes()
	{
		if(PlayerPrefs.HasKey("MUSICVOLUME"))
		{
			AudioManager.Audio.musicVolume = PlayerPrefs.GetFloat("MUSICVOLUME");
			AudioManager.Audio.music.volume = AudioManager.Audio.musicVolume;
		}
		
		if(PlayerPrefs.HasKey("VOICEVOLUME"))
		{
			AudioManager.Audio.voiceVolume = PlayerPrefs.GetFloat("VOICEVOLUME");
		}
		
		if(PlayerPrefs.HasKey("EFFECTSVOLUME"))
		{
			AudioManager.Audio.effectsVolume = PlayerPrefs.GetFloat("EFFECTSVOLUME");
			AudioManager.Audio.foleyVolume = AudioManager.Audio.effectsVolume;
		}
	}
	
	public void LoadVolumes(bool open)
	{
		if(open)
		{
			if(PlayerPrefs.HasKey("MUSICVOLUME"))
			{
				AudioManager.Audio.musicVolume = PlayerPrefs.GetFloat("MUSICVOLUME");
				AudioManager.Audio.music.volume = AudioManager.Audio.musicVolume;
				MusicSlider.value = AudioManager.Audio.music.volume;
			}
			
			if(PlayerPrefs.HasKey("VOICEVOLUME"))
			{
				AudioManager.Audio.voiceVolume = PlayerPrefs.GetFloat("VOICEVOLUME");
				VoiceSlider.value = AudioManager.Audio.voiceVolume;
			}
			
			if(PlayerPrefs.HasKey("EFFECTSVOLUME"))
			{
				AudioManager.Audio.effectsVolume = PlayerPrefs.GetFloat("EFFECTSVOLUME");
				AudioManager.Audio.foleyVolume = AudioManager.Audio.effectsVolume;
				EffectsSlider.value = AudioManager.Audio.effectsVolume;
			}
		}
	}
	
	#endregion AUDIO 
	
	#region Difficulty
	
	public void _Easydifficulty()
	{
		easyTrace.SetActive(true);
		moderateTrace.SetActive(false);
		hardTrace.SetActive(false);
		
		GameMode.GM._difficulty = GameMode.Difficulty.easy;
	}
	
	public void _ModerateDifficulty()
	{
		easyTrace.SetActive(false);
		moderateTrace.SetActive(true);
		hardTrace.SetActive(false);
		
		GameMode.GM._difficulty = GameMode.Difficulty.moderate;
	}
	
	public void _HardDifficulty()
	{
		easyTrace.SetActive(false);
		moderateTrace.SetActive(false);
		hardTrace.SetActive(true);
		
		GameMode.GM._difficulty = GameMode.Difficulty.hard;
	}
	
	private void SaveDifficulty()
	{
		PlayerPrefs.SetString("Difficulty", GameMode.GM._difficulty.ToString());
	}
	
	private void LoadDifficulty()
	{
		if(PlayerPrefs.HasKey("Difficulty"))
		{
			string difficultyLoad = PlayerPrefs.GetString("Difficulty");
			
			if(difficultyLoad == GameMode.Difficulty.easy.ToString())
			{
				GameMode.GM._difficulty = GameMode.Difficulty.easy;
			}
			else if(difficultyLoad == GameMode.Difficulty.moderate.ToString())
			{
				GameMode.GM._difficulty = GameMode.Difficulty.moderate;
			}
			else if(difficultyLoad == GameMode.Difficulty.hard.ToString())
			{
				GameMode.GM._difficulty = GameMode.Difficulty.hard;
			}
		}
	}
	
	private void LoadDifficulty(bool open) // o Open significa que foi aberto junto com o gameobject do configuration, ai ele passa o valor para o Sliders
	{
		if(open)
		{
			if(PlayerPrefs.HasKey("Difficulty"))
			{
				string difficultyLoad = PlayerPrefs.GetString("Difficulty");
				
				if(difficultyLoad == GameMode.Difficulty.easy.ToString())
				{
					_Easydifficulty();
				}
				else if(difficultyLoad == GameMode.Difficulty.moderate.ToString())
				{
					_ModerateDifficulty();
				}
				else if(difficultyLoad == GameMode.Difficulty.hard.ToString())
				{
					_HardDifficulty();
				}
			}
		}
	}
	
	#endregion Difficulty
}
