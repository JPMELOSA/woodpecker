using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TransitionSceneManager : MonoBehaviour
{
	#region STATIC TRANSITION SCENE MANAGER
	public static TransitionSceneManager TSC;

	private void OnEnable()
	{
		if (TransitionSceneManager.TSC == null)
		{
			TransitionSceneManager.TSC = this;
		}
		else
		{
			if (TransitionSceneManager.TSC != this)
			{
				Destroy(this.gameObject);
			}
		}
		DontDestroyOnLoad(this.gameObject);
	}
	#endregion STATIC TRANSITION SCENE MANAGER

	[SerializeField] private Animator fade;
	
	//AsyncOperation sceneLoading;

	void Start()
	{
		fade.SetBool("Fade", true);
	}

	#region Scene
	public void TransitionScene(string sceneName, float time = 1.5f)
	{
		StartCoroutine(Please(sceneName, time));
	}

	IEnumerator Please(string sceneName, float time)
	{
		fade.SetBool("Fade", false);
		yield return new WaitForSeconds(time);
		AsyncOperation sceneLoading = SceneManager.LoadSceneAsync(sceneName);
		
		while (!sceneLoading.isDone)
		{
			//progresso = (int) (carregamento.progress * 100.0f);
			yield return null;
		}
		
		//SceneManager.LoadScene(sceneName);
		fade.SetBool("Fade", true);
	}
	
	#endregion Scene

	#region Canvas
	public void TransitionCanvasMenu()
	{
		fade.SetBool("Fade", true);
		StartCoroutine(PleaseCanvas());
	}

	IEnumerator PleaseCanvas()
	{
		yield return new WaitForSeconds(1);
	}
	#endregion Canvas
}
