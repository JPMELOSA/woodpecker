using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallIntoTheHole : MonoBehaviour
{
	[Header("Menu Restart")]
	[SerializeField] private GameObject menuRestart;
	
	[Header("Camera")]
	[SerializeField] private Camera _camera;
	
	
	private void OnTriggerEnter2D(Collider2D other) 
	{
		if(other.tag == "Player")
		{
			Stats.stats.gameOver = true; // talvez colocar isso no menu restart, para toda vez que ele for chamado, der uma game over
			menuRestart.SetActive(true);
			// Desativa a camera de seguir o Player
		}
	}
	
}
