using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour
{
	#region STATIC MENU MANAGER
	public static PauseManager pause;

	private void OnEnable()
	{
		if (PauseManager.pause == null)
		{
			PauseManager.pause = this;
		}
		else
		{
			if (PauseManager.pause != this)
			{
				Destroy(this.gameObject);
			}
		}
		//DontDestroyOnLoad(this.gameObject);
	}
	#endregion STATIC MENU MANAGER

	[Header("Pause Menu")]
	[SerializeField] private GameObject pauseMenu;
	
	[SerializeField] private bool isPause = false;
	
	private void Update() 
	{
		if(Input.GetKeyDown(KeyCode.P))
		{
			isPause = !isPause;
			PauseButtonP(isPause);
		}
	}
	
	private void PauseButtonP(bool pause)
	{
		if(pause)
		{
			pauseMenu.SetActive(true);
			Time.timeScale = 0;
		}
		else
		{
			
			pauseMenu.SetActive(false);
			Time.timeScale = 1;
		}
	}
	
	public void _QuitGame()
	{
		Application.Quit();
	}
	
	public void _MainMenu()
	{
		TransitionSceneManager.TSC.TransitionScene("02-MainMenu", 2);
	}
	
	public void _Restart()
	{
		Time.timeScale = 1;
		Scene scene = SceneManager.GetActiveScene();
		TransitionSceneManager.TSC.TransitionScene(scene.name, 2);
	}
}
