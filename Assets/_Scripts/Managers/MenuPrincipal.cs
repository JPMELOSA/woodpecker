﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuPrincipal : MonoBehaviour
{
	#region STATIC MENU MANAGER
	public static MenuPrincipal Menu;

	private void OnEnable()
	{
		if (MenuPrincipal.Menu == null)
		{
			MenuPrincipal.Menu = this;
		}
		else
		{
			if (MenuPrincipal.Menu != this)
			{
				Destroy(this.gameObject);
			}
		}
		//DontDestroyOnLoad(this.gameObject);
	}
	#endregion STATIC MENU MANAGER

	[Header("Screens Menu Principal")]

	[SerializeField] private GameObject classicScreen;
	
	[SerializeField] private GameObject menuScreen;

	[SerializeField] private GameObject phaseSelectionScreen;

	[SerializeField] private GameObject creditsScreen;
	
	[Header("Time to change screens ")]
	
	[Range(0, 5)]
	[SerializeField] private  float timeChangeScreens = 1;
	
	
	private void Start() 
	{
		ChangeScreens();
		//AudioManager.Audio.LoadVolumes();
	}

	#region Scenes
	public void PlayGame()
	{
		AudioManager.Audio.PlayEffects("MOUSECLICK");
		TransitionSceneManager.TSC.TransitionScene("03-LoadScene");
	}

	public void PlayGameBonus()
	{
		AudioManager.Audio.PlayEffects("MOUSECLICK");
		TransitionSceneManager.TSC.TransitionScene("06-Bonus");
	}

	public void PlayGameCataratas()
	{
		AudioManager.Audio.PlayEffects("MOUSECLICK");
		TransitionSceneManager.TSC.TransitionScene("05-Barril");
	}

	public void PlayMenuPrincipal()
	{
		AudioManager.Audio.PlayEffects("MOUSECLICK");
		TransitionSceneManager.TSC.TransitionScene("02-MainMenu");
	}
	
	public void PlayEverglades()
	{
		AudioManager.Audio.PlayEffects("MOUSECLICK");
		TransitionSceneManager.TSC.TransitionScene("04-Everglade");
	}
	
	public void _playMainMenuCredits(bool active) // Aqui
	{
		AudioManager.Audio.PlayEffects("MOUSECLICK");
		menuScreen.SetActive(active);
		creditsScreen.SetActive(!active); 
	}
	
	public void _GoToPhaseSelection(bool active)// Aqui
	{
		AudioManager.Audio.PlayEffects("MOUSECLICK");
		menuScreen.SetActive(active);
		phaseSelectionScreen.SetActive(!active);
	}
	
	public void _QuitGame()
	{
		AudioManager.Audio.PlayEffects("MOUSECLICK");
		Application.Quit();
	}
	
	private void ChangeScreens() 
	{
		AudioManager.Audio.PlayMusic("MAINMENU");
		classicScreen.SetActive(true);
		StartCoroutine(ChangeMainMenu());
	}
	
	IEnumerator ChangeMainMenu()
	{
		yield return new WaitForSeconds(timeChangeScreens);
		classicScreen.SetActive(false);
		menuScreen.SetActive(true);
	}
	#endregion Scenes
	
}