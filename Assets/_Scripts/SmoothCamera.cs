﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothCamera : MonoBehaviour
{
	[SerializeField]
	private Transform target;

	[SerializeField]
	private Vector3 offSet;

	[Range(1, 100)] [SerializeField]
	private float smoothFactor;


	void Start()
	{
		offSet.z = -1;
		smoothFactor = 5;
	}
	private void FixedUpdate()
	{
		follow();
	}

	void follow()
	{
		Vector3 targetPosition;
		if(Stats.stats.gameOver)
		{
			targetPosition = new Vector3(target.position.x, transform.position.y, transform.position.z);
		}
		else
		{
			targetPosition = target.position + offSet;
		}
		Vector3 smoothedPosition = Vector3.Lerp(transform.position, targetPosition, smoothFactor * Time.fixedDeltaTime);
		transform.position = smoothedPosition;
	}
}
