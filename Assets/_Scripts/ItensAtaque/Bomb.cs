﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
	[Range(100, 5000)]
	[SerializeField] private float propulsion = 400f;

	[Range(0.5f, 10)]
	[SerializeField] private float timeToDetonation = 2f;
	
	[Range(0.05f, 5)]
	[SerializeField] private float explosionDuration =  2.5f;
	
	
	[Header("Add")]

	[SerializeField] private GameObject explosionAnim;

	[SerializeField] private GameObject explosionRay;
	
	Rigidbody2D rigid;
	
	void Start()
	{
		rigid = GetComponent<Rigidbody2D>();

		if(Stats.stats.isRight)
		{
			rigid.AddForce(Vector2.right * propulsion * Time.fixedDeltaTime, ForceMode2D.Impulse);
		}
		else
		{
			rigid.AddForce(Vector2.left * propulsion * Time.fixedDeltaTime, ForceMode2D.Impulse);
		}
		StartCoroutine(Count());
		Destroy(this.gameObject, explosionDuration);
	}

	private IEnumerator Count()
	{
		yield return new WaitForSeconds(timeToDetonation);
		this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
		GameObject explosao = Instantiate(explosionAnim, transform.position, Quaternion.identity);
		Destroy(explosao, 0.5f);
		GetComponent<AudioSource>().Play(); 
		explosionRay.SetActive(true);
	}
}
