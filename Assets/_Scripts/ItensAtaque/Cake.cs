﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cake : MonoBehaviour
{
	[Range(100, 5000)]
	[SerializeField] private float propulsion = 400f;
	
	[Range(1, 10)]
	[SerializeField] private float cakeDuration = 4f;
	
	[Range(0, 5)]
	[SerializeField] private float explosionDuration =  0.55f;
	

	[Header("ADD")]
	[SerializeField] private GameObject regularCake;

	[SerializeField] private GameObject cakeExploding;

	[SerializeField] private GameObject explosionRay;
	
	
	Rigidbody2D rigid;

	void Start()
	{
		rigid = GetComponent<Rigidbody2D>();
		
		if(Stats.stats.isRight)
		{
			rigid.AddForce(Vector2.right * propulsion * Time.fixedDeltaTime, ForceMode2D.Impulse);
		}
		else
		{
			rigid.AddForce(Vector2.left * propulsion * Time.fixedDeltaTime, ForceMode2D.Impulse);
		}
		Destroy(this.gameObject, cakeDuration);
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		switch(other.tag)
		{
			case "Enemy":
			{
				explosionRay.SetActive(true);
				rigid.freezeRotation = true;
				regularCake.SetActive(false);
				cakeExploding.SetActive(true);
				Destroy(this.gameObject, explosionDuration);
				break;
			}
			default:
			{
				break;
			}
		}
	}
}
