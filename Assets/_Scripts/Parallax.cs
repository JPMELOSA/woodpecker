﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    private float length;
    private float StarPos;

    private Transform can;

    public float parallaxEffect;
    void Start()
    {
        StarPos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
        can = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        float rePos = can.transform.position.x * (1 - parallaxEffect);
        float distance = can.transform.position.x * parallaxEffect;
        transform.position = new Vector3(StarPos + distance, transform.position.y, transform.position.z);

        if(rePos > StarPos + length)
        {
            StarPos += length;
        }
        else if(rePos < StarPos - length)
        {
            StarPos -= length;
        }
    }
}
