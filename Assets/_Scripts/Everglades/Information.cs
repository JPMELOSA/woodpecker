﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Information : MonoBehaviour
{
    [SerializeField]
    private GameObject titulo1;

    [SerializeField]
    private GameObject titulo2;

    [SerializeField]
    private GameObject painel;

    // Páginas

    [SerializeField]
    private GameObject pagina1;

    [SerializeField]
    private GameObject pagina2;

    [SerializeField]
    private GameObject pagina3;

    //Information _informacao;

    public void mudaPagina1(bool opcao)
    {
        // fecha página 1 e abre a página 2
        pagina1.SetActive(!opcao);
        pagina2.SetActive(opcao); // considedrando a variável bool verdadeira
    }

    public void mudaPagina2(bool opcao)
    {
        pagina2.SetActive(!opcao);
        titulo1.SetActive(!opcao);
        pagina3.SetActive(opcao);
        titulo2.SetActive(opcao);
    }

    public void fecharPagina()
    {
        painel.SetActive(false);
        Stats.stats.gameOver = false;
    }

    public void fecharPanel(bool opcao)
    {
        painel.SetActive(!opcao);
    }
}
