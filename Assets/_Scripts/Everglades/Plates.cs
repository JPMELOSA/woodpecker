using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plates : MonoBehaviour
{
    [SerializeField] private PlateType plate;

    private bool used = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            if (!used)
            {
                switch (plate)
                {
                    case PlateType.FreeBoat:
                        {
                            AudioManager.Audio.PlayVoices("BARCOGRATIS");
                            break;
                        }
                    case PlateType.TouristInformation:
                        {
                            AudioManager.Audio.PlayVoices("INFORMATION");
                            break;
                        }
                    case PlateType.workForTourists:
                        {
                            AudioManager.Audio.PlayVoices("BICOPARATURISTAS");
                            break;
                        }
                    default:
                        {
                            Debug.LogError("Placas - Op��o Inv�lida");
                            break;
                        }
                }
            }
            used = true;
        }
    }
}

enum PlateType 
{
    FreeBoat,
    TouristInformation,
    workForTourists
}

