using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InitializeEverglades : MonoBehaviour
{
	void Start()
	{
		if(SceneManager.GetActiveScene().name == "04-Everglade")
		{
			AudioManager.Audio.PlayMusic("EVERGLADES");
			ButtonManager.BTN.StartGameMode();
		}
	}
}
