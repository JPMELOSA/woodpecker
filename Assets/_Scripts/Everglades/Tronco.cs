﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tronco : MonoBehaviour
{
    [SerializeField]
    private float speed = 15;
    [SerializeField]
    private float movimento;

    Rigidbody2D rigid;

    void Start()
    {
        movimento = 1f;
        rigid = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        movimentacao();
    }

    private void movimentacao()
    {
        limiteMovimentacao();
        rigid.velocity = new Vector2(rigid.velocity.x, movimento * speed);
    }
    private void limiteMovimentacao()
    {
        if(transform.position.y > 55) // Tanhamo Máximo
        {
            movimento = -1;
        }
        if(transform.position.y < 37) // Tamanho Mínimo
        {
            movimento = 1;
        }
        //rigid.velocity = new Vector2(movimento * speed * Time.fixedDeltaTime, rigid.velocity.y);      
    }
}
