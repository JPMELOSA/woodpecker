using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckFinal : MonoBehaviour
{
	void OnTriggerEnter2D(Collider2D other) 
	{
		if(other.tag == "Player")
		{
			if(Stats.stats.GetBags() >= 5)
			{
				// Fechar de forma circular a tela
				TransitionSceneManager.TSC.TransitionScene("02-MainMenu", 2); // mandar para uma fase de vizualizar o score e as conquistas
			}
			else
			{
				Debug.LogError("FALTOU UMA BOLSA");
				// falar uma mensagem que está falando Bolsas?
			}
		}	
	}
}
