﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvergledesRespawManager : MonoBehaviour
{
	[Header("Collectibles")]
	[SerializeField] private GameObject bomb;

	[SerializeField] private GameObject cake;

	[SerializeField] private GameObject strawberryCake;

	[SerializeField] private GameObject pinkEyeWhiskey;
	
	[SerializeField] private GameObject cacto;



	[Header("Ducks")]
	[SerializeField] private GameObject horizontalDuck;
	[SerializeField] private GameObject verticalDuck; 
	[SerializeField] private GameObject horizontalInvertedDuck; 


	[Header("Positions")]
	
	[Header("Collectibles Positions")]
	
	[SerializeField] private Vector2[] bombsPositions;
	
	[SerializeField] private Vector2[] cakesPositions;
	
	[SerializeField] private Vector2[] strawberryCakePositions;
	
	[SerializeField] private Vector2[] pinkEyeWhiskeyPositions;
	
	
	[Header("Ducks Positions")]
	[SerializeField] private Vector2[] HorizontalDuckPositions;
	
	[SerializeField] private Vector2[] HorizontalInvertedDuckPositions;
	
	[SerializeField] private Vector2[] VerticalDuckPositions;
	
	
	private GameObject moeda; // se caso fizer


	void Start()
	{
		Bombs();
		Cakes();
		StrawberryCake();
		PinkEyeWhiskey();
		//
		Moedas();
		Cactus();
		Ducks();
	}

	#region Collectibles	
	
	private void Bombs()
	{
		for(int i = 0; i < bombsPositions.Length; i++)
		{
			Instantiate(bomb, bombsPositions[i], Quaternion.identity);
		}
	}

	private void Cakes()
	{
		for(int i = 0; i < cakesPositions.Length; i++)
		{
			Instantiate(cake, cakesPositions[i], Quaternion.Euler(0, 0, 90));
		}
	}

	private void Moedas()
	{
		
	}

	private void StrawberryCake()
	{
		for(int i = 0; i < strawberryCakePositions.Length; i++)
		{
			Instantiate(strawberryCake, strawberryCakePositions[i], Quaternion.identity);
		}
	}

	private void PinkEyeWhiskey()
	{
		for(int i = 0; i < pinkEyeWhiskeyPositions.Length; i++)
		{
			Instantiate(pinkEyeWhiskey, pinkEyeWhiskeyPositions[i], Quaternion.identity);
		}
	}
	
	#endregion Collectibles
	
	#region Enemies 
	
	private void Cactus()
	{
		Instantiate(cacto, new Vector2(142.49f, 6.32f), Quaternion.identity);
		Instantiate(cacto, new Vector2(31.22f, -2.96f), Quaternion.identity);
		Instantiate(cacto, new Vector2(305.52f, 36.99f), Quaternion.identity);
		if(GameMode.GM._difficulty == GameMode.Difficulty.hard)
		{
			Instantiate(cacto, new Vector2(417.84f, 37f), Quaternion.identity);
			Instantiate(cacto, new Vector2(20.06f, -1f), Quaternion.identity); 
			Instantiate(cacto, new Vector2(102.11f, 4.66f), Quaternion.identity); 
			Instantiate(cacto, new Vector2(116.44f, 7.31f), Quaternion.identity); 
			Instantiate(cacto, new Vector2(223.2f, 5.29f), Quaternion.identity); 
			Instantiate(cacto, new Vector2(270.19f, 12.25f), Quaternion.identity);
			Instantiate(cacto, new Vector2(557.3f, 56.6f), Quaternion.identity);
			Instantiate(cacto, new Vector2(630.5f, 53.9f), Quaternion.identity);
		}
	}
	
	private void Ducks()
	{
		for(int i = 0; i < HorizontalDuckPositions.Length; i++)
		{
			Instantiate(horizontalDuck, HorizontalDuckPositions[i], Quaternion.identity);
		}

		for(int i = 0; i < HorizontalInvertedDuckPositions.Length; i++)
		{
			Instantiate(horizontalInvertedDuck, HorizontalInvertedDuckPositions[i], Quaternion.identity);
		}
		
		for(int i = 0; i < VerticalDuckPositions.Length; i++)
		{
			Instantiate(verticalDuck, VerticalDuckPositions[i], Quaternion.Euler(0, 180, 0));
		}
	}
	#endregion Enemies
}
