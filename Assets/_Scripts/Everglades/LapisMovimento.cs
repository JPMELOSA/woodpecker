﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LapisMovimento : MonoBehaviour
{
    float movimento;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        mudaPosicao();
    }

    private void mudaPosicao()
    {
        movimento = Input.GetAxisRaw("Horizontal");

        if (movimento > 0)
        {
            // Vira para Direita
            this.gameObject.transform.eulerAngles = new Vector2(0, 0);
        }
        if (movimento < 0)
        {
            //Vira para a Esquerda
            this.gameObject.transform.eulerAngles = new Vector2(0, 180);
        }
    }
}
