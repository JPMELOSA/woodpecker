using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Life : MonoBehaviour
{
	[Header("Lifes this Enemy")]
	[Range(1, 10)]
	[SerializeField] private int lifesNumber = 1;

	[Header("Enemy")]
	[SerializeField] private GameObject enemy;

	[Header("Award")]
	[SerializeField] private GameObject bag;
	
	[Header("Enemy Type")]
	[SerializeField] EnemyType enemyName;
	
	[Header("Enemy State")]
	public EnemyState enemyState;
	
	[Header("Enemy Walk")]
	[SerializeField] private GameObject enemyWalk;
	
	[Header("Enemy Dead")]
	[SerializeField] private GameObject enemyDead;
	
	[Header("Damage")]
	[SerializeField] private GameObject damage;
	
	private void Start() 
	{
		if(GameMode.GM._difficulty == GameMode.Difficulty.easy)
		{
			lifesNumber = 1;
		}
		else if(GameMode.GM._difficulty == GameMode.Difficulty.moderate)
		{
			lifesNumber = 2;
		}
		else if(GameMode.GM._difficulty == GameMode.Difficulty.hard)
		{
			lifesNumber = 5;
		}
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.tag == "Player" || collision.tag == "Explosao" || collision.tag == "Torta")
		{
			lifesNumber--;
			checkScream();
			ChangeColor();
			if(lifesNumber <= 0)
			{
				if(bag != null)
				{
					Instantiate(bag, transform.position, Quaternion.identity);
				}
				Destroy(damage);
				enemy.GetComponent<Enemy>().isDead = true;
				Destroy(enemyWalk);
				enemyDead.SetActive(true);
				Destroy(this.gameObject);
			}
		}
	}
	
	private void checkScream()
	{
		switch(enemyName)
		{
			case EnemyType.BuzzBuzzard:
			{
				if(lifesNumber <= 0)
				{
					AudioManager.Audio.PlayVoices("BuzzBuzzardGroan"); //
				}
				else
				{
					AudioManager.Audio.PlayVoices("BuzzBuzzardDead"); //  
				}
				break;
			}
			case EnemyType.Frank:
			{
				if(lifesNumber <= 0)
				{
					AudioManager.Audio.PlayVoices("FrankDead"); //
				}
				else
				{
					AudioManager.Audio.PlayVoices("FrankGroan"); //
				}
				break;
			}
			case EnemyType.GabbyGator:
			{
				if(lifesNumber <= 0)
				{
					AudioManager.Audio.PlayVoices("GABBYGATORDEAD"); // 
				}
				else
				{
					AudioManager.Audio.PlayVoices("GABBYGATORHEY"); // 
				}
				break;
			}
			case EnemyType.Jubileu:
			{
				if(lifesNumber <= 0)
				{
					AudioManager.Audio.PlayVoices("JUBILEUDEAD"); //
				}
				else
				{
					AudioManager.Audio.PlayVoices("FrankGroan"); //
				}
				break;
			}
			case EnemyType.Meany:
			{
				if(lifesNumber <= 0)
				{
					AudioManager.Audio.PlayVoices("MEANYDEAD"); //
				}
				else
				{
					AudioManager.Audio.PlayVoices("MEANYGROAN"); //
				}
				break;
			}
			case EnemyType.WallyWalrus:
			{
				if(lifesNumber <= 0)
				{
					AudioManager.Audio.PlayVoices("WALLYDEAD"); //
				}
				else
				{
					AudioManager.Audio.PlayVoices("WALLYSPAT"); //
				}
				break;
			}
			case EnemyType.WildBillHiccup:
			{
				if(lifesNumber <= 0)
				{
					AudioManager.Audio.PlayVoices("WILDDEAD"); //
				}
				else
				{
					AudioManager.Audio.PlayVoices("WILDGROAN"); //
				}
				break;
			}
			case EnemyType.Doley:
			{
				if(lifesNumber <= 0)
				{
					AudioManager.Audio.PlayEffects("StrongBeat"); //
				}
				else
				{
					AudioManager.Audio.PlayVoices("DOLEY1"); //
				}
				break;
			}
		}
	}

	private void ChangeColor()
	{
		enemyWalk.GetComponent<SpriteRenderer>().color = Color.red;
		StartCoroutine(BackColor());
	}

	IEnumerator BackColor()
	{
		yield return new WaitForSeconds(0.2f);
		enemyWalk.GetComponent<SpriteRenderer>().color = Color.white;
	}
}

public enum EnemyState
{
	alive,
	dead
}

enum EnemyType 
{
	BuzzBuzzard,
	GabbyGator,
	WallyWalrus,
	Meany,
	WildBillHiccup,
	Jubileu,
	Frank,
	Doley
}
