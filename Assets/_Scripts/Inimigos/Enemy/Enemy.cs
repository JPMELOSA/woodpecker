using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
	[Header("Enemy Movimentation")]
	[Range(0, 30)]
	[SerializeField] private float radiusOfMotion = 2;

	[Range(0, 30)]
	[SerializeField] private float speed = 5f;

	Rigidbody2D rigid;

	private float initialPosition;

	private float maxPosition;

	private float minPosition;

	private float movement = 0;

	public bool isDead = false;

	void Start()
	{
		rigid = GetComponent<Rigidbody2D>();
		movement = 1;
		initialPosition = transform.position.x;
		maxPosition = initialPosition + radiusOfMotion;
		minPosition = initialPosition - radiusOfMotion;
		
		if(GameMode.GM._difficulty == GameMode.Difficulty.easy)
		{
			speed = 3;
		}
		else if(GameMode.GM._difficulty == GameMode.Difficulty.moderate)
		{
			speed = 5;
		}
		else if(GameMode.GM._difficulty == GameMode.Difficulty.hard)
		{
			speed = 7;
		}
	}

	void Update()
	{
		movimentacao();
	}

	private void movimentacao()
	{
		if (!isDead)
		{
			MovementLimit();
			rigid.velocity = new Vector2(movement * speed, rigid.velocity.y);
			InvertImage();
		}
		else
		{
			rigid.constraints = RigidbodyConstraints2D.FreezeAll;
		}
	}
	private void MovementLimit()
	{
		if (transform.position.x < minPosition) 
		{
			movement = 1;
		}
		if (transform.position.x > maxPosition)
		{
			movement = -1;
		}
	}

	private void InvertImage()
	{
		if (movement < 0)
		{
			//vira para a esquerda
			transform.eulerAngles = new Vector2(0, 180);
		}
		if (movement > 0)
		{
			//vira para a direita
			transform.eulerAngles = new Vector2(0, 0);
		}
	}
	
	public void Stop()
	{
		movement = 0;
	}
}
