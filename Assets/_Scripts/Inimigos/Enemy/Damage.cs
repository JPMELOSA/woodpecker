using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour
{
	[Header("Damage")]
	[Range(1, 3)]
	[SerializeField] private int damage = 1;
	
	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.tag == "Player")
		{
			Stats.stats.RemoveLife(damage);
		}
	}
}

