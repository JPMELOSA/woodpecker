using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuckLife : MonoBehaviour
{
	[Header("Life")]
	[Range(1, 10)]
	[SerializeField] private int lifesNumber = 1;
	
	[Header("Duck")]
	[SerializeField] private GameObject duck;
	
	private void Start() 
	{
		if(GameMode.GM._difficulty == GameMode.Difficulty.easy)
		{
			lifesNumber = 5;
		}
		else if(GameMode.GM._difficulty == GameMode.Difficulty.moderate)
		{
			lifesNumber = 2;
		}
		else if(GameMode.GM._difficulty == GameMode.Difficulty.hard)
		{
			lifesNumber = 1;
		}
	}
	
	void OnTriggerEnter2D(Collider2D other) 
	{
		if(other.tag == "Player" || other.tag == "Explosao" || other.tag == "Torta")
		{
			lifesNumber--;
			ChangeColor();
			AudioManager.Audio.PlayVoices("DUCKGROAN");
			if(lifesNumber <= 0)
			{
				// colocar alguma animação de morte do Pato
				StartCoroutine(DeadDuck());
			}
		}
	}
	
	private void ChangeColor()
	{
		duck.GetComponent<SpriteRenderer>().color = Color.red;
		StartCoroutine(BackColor());
	}

	IEnumerator BackColor()
	{
		yield return new WaitForSeconds(0.2f);
		duck.GetComponent<SpriteRenderer>().color = Color.white;
	}
	
	IEnumerator DeadDuck()
	{
		yield return new WaitForSeconds(0.7f);
		Destroy(duck);
	}
}
