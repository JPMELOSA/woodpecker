using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuckMovement : MonoBehaviour
{
	[Header("Movement Type")]
	[SerializeField] private MovementType movementType;
	
	[Header("Distance")]
	[SerializeField] private float distance;
	
	[Header("Speed")]
	[Range(0, 30)]
	[SerializeField] private float speed = 5f;
	
	[Header("Start Movement By ")]
	[SerializeField] private MovementDirection initialDirection;
	
	Rigidbody2D rigid;
	private float initialPosition;

	private float maxPosition;

	private float minPosition;

	private float movement = 0;

	private bool isDead = false;
	void Start()
	{
		rigid = GetComponent<Rigidbody2D>();
		
		if(initialDirection == MovementDirection.Left)
		{
			movement = 1;
		}
		else
		{
			movement = -1;
		}
		
		
		if(movementType ==  MovementType.Horizontal)
		{
			initialPosition = transform.position.x;
			rigid.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
		}
		else if(movementType ==  MovementType.Vertical)
		{
			initialPosition = transform.position.y;
			rigid.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
		}
		maxPosition = initialPosition + distance;
		minPosition = initialPosition - distance;
		
		if(GameMode.GM._difficulty == GameMode.Difficulty.easy)
		{
			speed = 3;
		}
		else if(GameMode.GM._difficulty == GameMode.Difficulty.moderate)
		{
			speed = 5;
		}
		else if(GameMode.GM._difficulty == GameMode.Difficulty.hard)
		{
			speed = 7;
		}
	}

	void Update()
	{
		Movement();
	}
	
	private void Movement()
	{
		if (!isDead)
		{
			if(movementType == MovementType.Horizontal)
			{
				MovementHorizontalLimit();
				HorizontalMovement();
			}
			else if(movementType == MovementType.Vertical)
			{
				MovementVerticalLimit();
				VerticalMovement();
			}
			else if(movementType == MovementType.StraightVertical)
			{
				StraightVertical();
			}
		}
		else
		{
			rigid.IsSleeping();
		}
	}
	
	private void HorizontalMovement()
	{
		rigid.velocity = new Vector2(movement * speed, rigid.velocity.y);
		InvertImage();
	}
	
	private void VerticalMovement()
	{
		rigid.velocity = new Vector2(rigid.velocity.x, movement * speed);
	}
	
	private void StraightVertical()
	{
		
	}
	
	private void MovementHorizontalLimit()
	{
		if (transform.position.x < minPosition) 
		{
			movement = 1;
		}
		if (transform.position.x > maxPosition)
		{
			movement = -1;
		}
	}
	
	private void MovementVerticalLimit()
	{
		if (transform.position.y < minPosition) 
		{
			movement = 1;
		}
		if (transform.position.y > maxPosition)
		{
			movement = -1;
		}
	}
	
	private void InvertImage()
	{
		if (movement < 0)
		{
			//vira para a esquerda
			transform.eulerAngles = new Vector2(0, 180);
		}
		if (movement > 0)
		{
			//vira para a direita
			transform.eulerAngles = new Vector2(0, 0);
		}
	}
	
}

enum MovementType
{
	Vertical,
	StraightVertical,
	Horizontal,
	
	Stoped
}

enum MovementDirection 
{
	Left,
	Right
}
